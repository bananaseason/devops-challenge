## Getting started  

## Go
### Download Go dependencies
```bash
go mod init
go mod tidy
go get
```
### Build
```bash
go build
``` 
### Run
```bash
./devops-challenge
```

## Docker
### Build
```bash
docker build -t api .
```
### Run
```bash
docker run -it -p 8080:8080 api
```
