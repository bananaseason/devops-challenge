package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

const ApiVersion float32 = 1.1

type Response struct {
	Version float32
}

var JsonData []byte

// handleRequests function that matches the URL path with a defined function and
// a main function which will kick off API
func handleRequests() {
	// creates a new instance of a mux router
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/", homePage)
	myRouter.HandleFunc("/version", apiVersion)
	log.Fatal(http.ListenAndServe(":8080", myRouter))
}

// homePage is providing welcome message when querried
func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome!")
	fmt.Println("Endpoint Hit: homePage")
}

// apiVersion provides api version when querried
func apiVersion(w http.ResponseWriter, r *http.Request) {
	response := Response{ApiVersion}

	js, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
	fmt.Println("Endpoint Hit: apiVersion")
}

// main function calls all other functions
func main() {
	handleRequests()
}
